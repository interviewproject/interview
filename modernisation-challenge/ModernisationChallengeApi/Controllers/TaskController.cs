using Microsoft.AspNetCore.Mvc;
using ModernisationChallengeApi.Dtos;
using ModernisationChallengeApi.Services;

namespace ModernisationChallengeApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly ILogger<TaskController> _logger;
        public TaskController(ITaskService taskService, ILogger<TaskController> logger)
        {
            _taskService = taskService;
            _logger = logger;
        }
        /// <summary>
        /// Get all tasks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var tasks = await _taskService.Get();
            return Ok(tasks);
        }
        /// <summary>
        /// Get task by id
        /// </summary>
        /// <param name="id">task id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var tasks = await _taskService.GetTaskById(id);
            return Ok(tasks);
        }
        /// <summary>
        /// Update task based on task id
        /// </summary>
        /// <param name="id">task id</param>
        /// <param name="updateTaskDto">object to update</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateTaskDto updateTaskDto)
        {
            var isSucceed = await _taskService.Update(id, updateTaskDto);
            return Ok(isSucceed);
        }
        /// <summary>
        /// Update completed of the task based on task id
        /// </summary>
        /// <param name="id">task id</param>
        /// <param name="completed"></param>
        /// <returns></returns>
        [HttpPut("{id}/completed")]
        public async Task<IActionResult> UpdateCompleted([FromRoute] int id, [FromBody] UpdateCompletedTaskDto updateCompletedTaskDto)
        {
            var isSucceed = await _taskService.UpdateCompleted(id, updateCompletedTaskDto.Completed);
            return Ok(isSucceed);
        }
        /// <summary>
        /// Delete task based on task id
        /// </summary>
        /// <param name="id">task id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var isSucceed = await _taskService.Delete(id);
            return Ok(isSucceed);
        }
        /// <summary>
        /// Create a new task
        /// </summary>
        /// <param name="addTaskDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddTaskDto addTaskDto)
        {
            var taskId = await _taskService.Add(addTaskDto);
            return Ok(taskId);
        }
    }
}