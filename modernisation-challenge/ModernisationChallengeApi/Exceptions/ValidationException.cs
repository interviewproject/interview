﻿using System.Net;

namespace ModernisationChallengeApi.Exceptions
{
    public sealed class ValidationException : Exception
    {
        public int StatusCode { get; set; } = (int)HttpStatusCode.BadRequest;
        public string Message { get; set; }
        public List<ValidationError> Errors { get; }
        public ValidationException(string message)
        {
            Message = message;
        }
        public ValidationException(List<ValidationError> errors = null, string message = "fail")
        {
            Message = message;
            Errors = errors ?? new List<ValidationError>();
        }
    }
    public class ValidationError
    {
        public string Code { get; set; } = string.Empty;
        public string Field { get; set; }
        public string Message { get; set; }
        public ValidationError(string field, string message)
        {
            Field = field;
            Message = message;
        }
        public ValidationError(string field, string message, string code) : this(field, message)
        {
            Code = code;
        }
    }

}
