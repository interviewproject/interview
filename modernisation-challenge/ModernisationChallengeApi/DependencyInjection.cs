﻿using Microsoft.EntityFrameworkCore;
using ModernisationChallengeApi.Domain;
using ModernisationChallengeApi.Services;
using System;

namespace ModernisationChallengeApi
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddEntityFrameworkSqlServer()
               .AddDbContext<DatabaseContext>(options =>
                  options.UseSqlServer(configuration["ModernisationChallenge"]));
            services.AddScoped<DbContext>(provider => provider.GetService<DatabaseContext>());
            return services;
        }
        public static IServiceCollection RegisterService(this IServiceCollection services)
        {
            services.AddTransient<ITaskService, TaskService>();

            return services;
        }
        public static IServiceCollection AddCorsPolicy(this IServiceCollection services)
        {
            services.AddCors(options =>
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                }
            ));
            return services;
        }
    }
}
