﻿using Microsoft.EntityFrameworkCore;
using ModernisationChallengeApi.Domain.Entities;

namespace ModernisationChallengeApi.Domain
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tasks> Tasks { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //    => optionsBuilder.UseSqlServer("Data Source=(localdb)\\ProjectModels;Initial Catalog=ModernisationChallenge;");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Tasks>(entity =>
            {
                entity.Property(e => e.DateCreated).HasColumnType("datetime");
                entity.Property(e => e.DateDeleted).HasColumnType("datetime");
                entity.Property(e => e.DateModified).HasColumnType("datetime");
            });
        }

    }

}
