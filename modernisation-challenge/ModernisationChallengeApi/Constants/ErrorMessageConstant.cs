﻿namespace ModernisationChallengeApi.Constants
{
    public static class ErrorMessageConstant
    {
        public const string RequireDetailFieldOfTask = "One or more required fields haven't been filled in.";
    }
}
