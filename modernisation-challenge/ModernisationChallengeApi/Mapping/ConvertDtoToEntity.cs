﻿using ModernisationChallengeApi.Domain.Entities;
using ModernisationChallengeApi.Dtos;

namespace ModernisationChallengeApi.Mapping
{
    public static class ConvertDtoToEntity
    {
        public static Tasks ToCreateTask(this AddTaskDto addTaskDto) => new Tasks
        {

            DateCreated = DateTime.Now,
            DateModified = DateTime.Now,
            Details = addTaskDto.Details,
            Completed = false
        };
    }
}
