﻿using ModernisationChallengeApi.Domain.Entities;
using ModernisationChallengeApi.Dtos;

namespace ModernisationChallengeApi.Mapping
{
    public static class ConvertEntityToDto
    {
        public static TaskDto ToTaskDto(this Tasks entity) => new TaskDto
        {
            Id = entity.Id,
            Completed = entity.Completed,
            Details = entity.Details
        };
    }
}
