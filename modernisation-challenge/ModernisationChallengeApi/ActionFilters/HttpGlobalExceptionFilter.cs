﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using ModernisationChallengeApi.Exceptions;

namespace ModernisationChallengeApi.ActionFilters
{
    public class HttpGlobalExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;
        private readonly IDictionary<Type, Action<ExceptionContext>> _exceptionHandlers;

        public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger)
        {
            _logger = logger;
            // Register known exception types and handlers.
            _exceptionHandlers = new Dictionary<Type, Action<ExceptionContext>>
            {
                { typeof(ValidationException), HandleValidationException },
                { typeof(NotFoundException), HandleNotFoundException },
            };
        }
        public override void OnException(ExceptionContext context)
        {
            HandleException(context);
            base.OnException(context);
        }
        private void HandleException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                    context.Exception,
                    context.Exception.Message);
            Type type = context.Exception.GetType();
            if (_exceptionHandlers.ContainsKey(type))
            {
                _exceptionHandlers[type].Invoke(context);
                return;
            }
            if (!context.ModelState.IsValid)
            {
                HandleInvalidModelStateException(context);
                return;
            }
            HandleUnknownException(context);
        }
        private void HandleValidationException(ExceptionContext context)
        {
            var exception = context.Exception as ValidationException;
            var response = new JsonErrorResponse
            {
                Messages = new[] { exception.Message },
                ValidationError = exception.Errors,
                Title = "Validation"
            };
            context.Result = new BadRequestObjectResult(response);
            context.ExceptionHandled = true;
        }

        private void HandleInvalidModelStateException(ExceptionContext context)
        {
            var details = new ValidationProblemDetails(context.ModelState);
            context.Result = new BadRequestObjectResult(details);
            context.ExceptionHandled = true;
        }

        private void HandleNotFoundException(ExceptionContext context)
        {
            var exception = context.Exception as NotFoundException;
            var response = new JsonErrorResponse("The specified resource was not found.");
            context.Result = new NotFoundObjectResult(response);
            context.ExceptionHandled = true;
        }
        private void HandleUnknownException(ExceptionContext context)
        {
            var response = new JsonErrorResponse("An error occurred while processing your request.");
            context.Result = new ObjectResult(response)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            context.ExceptionHandled = true;
        }
        private class JsonErrorResponse
        {
            public string[] Messages { get; set; }
            public object ValidationError { get; set; }
            public string Title { get; set; }
            public JsonErrorResponse()
            {

            }
            public JsonErrorResponse(string title)
            {
                Title = title;
            }
        }
    }
}
