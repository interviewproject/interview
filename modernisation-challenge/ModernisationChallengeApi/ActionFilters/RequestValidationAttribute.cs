﻿using Microsoft.AspNetCore.Mvc.Filters;
using ModernisationChallengeApi.Exceptions;

namespace ModernisationChallengeApi.ActionFilters
{
    public class RequestValidationAttribute : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var modelState = context.ModelState;
            if (!modelState.IsValid)
            {
                var errorDetails = new List<ValidationError>();
                foreach (var key in modelState.Keys)
                {
                    errorDetails.Add(new ValidationError(key, modelState[key].Errors.FirstOrDefault()?.ErrorMessage));
                }
                throw new Exceptions.ValidationException(errorDetails);
            }
            await next();
        }
    }

}
