﻿using ModernisationChallengeApi.Constants;
using System.ComponentModel.DataAnnotations;

namespace ModernisationChallengeApi.Dtos
{
    public class AddTaskDto
    {
        [Required(ErrorMessage = ErrorMessageConstant.RequireDetailFieldOfTask)]
        public string Details { get; set; }
    }
}
