﻿namespace ModernisationChallengeApi.Dtos
{
    public class TaskDto
    {
        public int Id { get; set; }
        public bool Completed { get; set; }
        public string Details { get; set; }
    }
}
