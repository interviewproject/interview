﻿namespace ModernisationChallengeApi.Dtos
{
    public class UpdateCompletedTaskDto
    {
        public bool Completed { get; set; }
    }
}
