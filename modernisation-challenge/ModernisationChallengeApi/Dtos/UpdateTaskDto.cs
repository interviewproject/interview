﻿using ModernisationChallengeApi.Constants;
using System.ComponentModel.DataAnnotations;

namespace ModernisationChallengeApi.Dtos
{
    public class UpdateTaskDto
    {
        [Required(ErrorMessage = ErrorMessageConstant.RequireDetailFieldOfTask)]
        public string Details { get; set; }
    }
}
