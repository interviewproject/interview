﻿using Microsoft.EntityFrameworkCore;
using ModernisationChallengeApi.Domain;
using ModernisationChallengeApi.Domain.Entities;
using ModernisationChallengeApi.Dtos;
using ModernisationChallengeApi.Exceptions;
using ModernisationChallengeApi.Mapping;

namespace ModernisationChallengeApi.Services
{
    public class TaskService : ITaskService
    {
        private readonly DatabaseContext _dbContext;
        public TaskService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<TaskDto>> Get()
        {
            var tasks = from task in _dbContext.Tasks
                        where task.DateDeleted == null
                        orderby task.Id
                        select new TaskDto
                        {
                            Id = task.Id,
                            Completed = task.Completed,
                            Details = task.Details
                        };
            return await tasks.ToListAsync();
        }
        public async Task<TaskDto> GetTaskById(int taskId)
        {
            var task = await _dbContext.Tasks.AsNoTracking().SingleOrDefaultAsync(x => x.Id == taskId);
            if (task is null)
            {
                return new TaskDto();
            }
            return task.ToTaskDto();
        }
        public async Task<int> Add(AddTaskDto addTaskDto)
        {
            var task = new Tasks
            {
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                Details = addTaskDto.Details,
                Completed = false
            };
            _dbContext.Tasks.Add(task);
            await _dbContext.SaveChangesAsync();
            return task.Id;
        }
        public async Task<bool> Update(int taskId, UpdateTaskDto updateTaskDto)
        {
            var task = _dbContext.Tasks.SingleOrDefault(x => x.Id == taskId);
            if (task is null)
            {
                throw new NotFoundException($"Could not found a task with id {taskId}");
            }
            task.Details = updateTaskDto.Details;
            task.DateModified = DateTime.Now;
            _dbContext.Update(task);
            var isSucceed = await _dbContext.SaveChangesAsync() > 0;
            return isSucceed;
        }
        public async Task<bool> UpdateCompleted(int taskId, bool completed)
        {
            var task = _dbContext.Tasks.SingleOrDefault(x => x.Id == taskId);
            if (task is null)
            {
                throw new NotFoundException($"Could not found a task with id {taskId}");
            }
            task.Completed = completed;
            task.DateModified = DateTime.Now;
            _dbContext.Update(task);
            var isSucceed = await _dbContext.SaveChangesAsync() > 0;
            return isSucceed;
        }
        public async Task<bool> Delete(int taskId)
        {
            var task = _dbContext.Tasks.Single(x => x.Id == taskId);
            task.DateDeleted = DateTime.Now;
            var isSucceed = await _dbContext.SaveChangesAsync() > 0;
            return isSucceed;
        }
    }
}
