﻿using ModernisationChallengeApi.Dtos;

namespace ModernisationChallengeApi.Services
{
    public interface ITaskService
    {
        /// <summary>
        /// Get all tasks
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TaskDto>> Get();
        /// <summary>
        /// Get task by task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<TaskDto> GetTaskById(int taskId);
        /// <summary>
        /// Create a new task
        /// </summary>
        /// <param name="tasks"></param>
        /// <returns></returns>
        Task<int> Add(AddTaskDto tasks);
        /// <summary>
        /// Update task
        /// </summary>
        /// <returns></returns>
        Task<bool> Update(int taskId,UpdateTaskDto updateTaskDto);
        /// <summary>
        /// Update completed of task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="Completed"></param>
        /// <returns></returns>
        Task<bool> UpdateCompleted(int taskId, bool completed);
        /// <summary>
        /// Delete task base on task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<bool> Delete(int taskId);
    }
}
