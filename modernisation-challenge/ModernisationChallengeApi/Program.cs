using Microsoft.AspNetCore.Mvc;
using ModernisationChallengeApi;
using ModernisationChallengeApi.ActionFilters;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true;
});
builder.Services.AddControllers(options =>
{
    options.Filters.Add<RequestValidationAttribute>();
    options.Filters.Add<HttpGlobalExceptionFilter>();
})
;
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen()
    .AddCorsPolicy()
    .AddPersistence(builder.Configuration)
    .RegisterService();
var app = builder.Build();
app.UseCors();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
