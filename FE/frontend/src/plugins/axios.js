import axios from "axios";
const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  headers: {
    common: {
      "Content-Type": "application/json; charset=utf-8",
    },
  },
  timeout: 15000,
});

axiosInstance.interceptors.request.use((request) => {
  // if set token in header then it will to do at here
  return request;
});

axiosInstance.interceptors.response.use(
  (response) => {
    return { data: response.data, success: true };
  },
  (error) => {
    let errorInfo = {
      code: "Undefined",
      message: "Server error",
    };

    if (error.response) {
      const { status, data } = error.response;
      errorInfo.code = status;
      errorInfo.message = data;
    }
    return { error: errorInfo };
  }
);
export default axiosInstance;
