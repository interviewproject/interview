import { apiGenerator } from "@/helpers/api-helper";
import { API_NAME } from "@/configs/api-config";

export default (axios) => ({
  create(payload) {
    const api = apiGenerator(axios, API_NAME.ADD_TASK, { payload });
    return api.call();
  },
  get(payload) {
    const api = apiGenerator(axios, API_NAME.GET_TASKs, { payload });
    return api.call();
  },
  getById(id) {
    const api = apiGenerator(axios, API_NAME.GET_TASK_BY_ID, {
      params: { id },
    });
    return api.call();
  },
  update(id, payload) {
    const api = apiGenerator(axios, API_NAME.UPDATE_TASK, {
      params: { id },
      payload,
    });
    return api.call();
  },
  updateCompleted(id, payload) {
    const api = apiGenerator(axios, API_NAME.UPDATE_COMPLETED_TASK, {
      params: { id },
      payload,
    });
    return api.call();
  },
  delete(id) {
    const api = apiGenerator(axios, API_NAME.DELETE_TASK_BY_ID, {
      params: { id },
    });
    return api.call();
  },
});
