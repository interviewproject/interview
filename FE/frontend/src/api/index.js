import Task from "./task";

export default (context) => {
  const repositories = {
    task: Task(context.axios),
  };
  context.provide("api", repositories);
};
