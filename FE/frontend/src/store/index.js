import { createStore } from "vuex";
import task from "./task";

export default createStore({
  modules: {
    task,
  },
});
