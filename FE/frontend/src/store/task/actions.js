const setTask = (context, data) => {
  context.commit("setTask", data);
};
export default {
  setTask,
};
