const getTasks = (state) => {
  return state.tasks;
};

export default {
  getTasks,
};
