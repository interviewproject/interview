const setTask = (state, data) => {
  state.tasks = data;
};

export default {
  setTask,
};
