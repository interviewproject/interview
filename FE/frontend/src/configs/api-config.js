export const API_NAME = {
  GET_TASKs: "GET_TASK",
  GET_TASK_BY_ID: "GET_TASK_BY_ID",
  ADD_TASK: "ADD_TASK",
  UPDATE_TASK: "UPDATE_TASK",
  UPDATE_COMPLETED_TASK: "UPDATE_COMPLETED_TASK",
  DELETE_TASK_BY_ID: "DELETE_TASK_BY_ID",
};
const MODULE = {
  TASK: "task",
};
export default {
  [API_NAME.GET_TASKs]: {
    module: MODULE.TASK,
    endpoint: "",
    version: "v1",
    method: "GET",
  },
  [API_NAME.GET_TASK_BY_ID]: {
    module: MODULE.TASK,
    endpoint: "${ id }",
    version: "v1",
    method: "GET",
  },
  [API_NAME.ADD_TASK]: {
    module: MODULE.TASK,
    endpoint: "",
    version: "v1",
    method: "POST",
  },
  [API_NAME.UPDATE_TASK]: {
    module: MODULE.TASK,
    endpoint: "${ id }",
    version: "v1",
    method: "PUT",
  },
  [API_NAME.UPDATE_COMPLETED_TASK]: {
    module: MODULE.TASK,
    endpoint: "${ id }/completed",
    version: "v1",
    method: "PUT",
  },
  [API_NAME.DELETE_TASK_BY_ID]: {
    module: MODULE.TASK,
    endpoint: "${ id }",
    version: "v1",
    method: "DELETE",
  },
};
