import { ref, inject } from "vue";
import store from "@/store";
export const useTasks = () => {
  const api = inject("api");
  const loading = ref(false);
  const result = ref([]);
  const errorMessage = ref("");

  const getTasks = async () => {
    loading.value = true;
    result.value = "";
    const response = await api.task.get();
    if (response.success) {
      store.dispatch("task/setTask", response.data);
    }
    loading.value = false;
  };
  const createTask = async (payload) => {
    result.value = "";
    loading.value = true;
    const response = await api.task.create(payload);
    if (response.success) {
      await getTasks();
    }
    loading.value = false;
  };
  const updateTask = async (id, payload) => {
    result.value = "";
    loading.value = true;

    const response = await api.task.update(id, payload);
    if (response.success) {
      await getTasks();
    }
    loading.value = false;
  };
  const updateCompletedTask = async (id, payload) => {
    result.value = "";
    loading.value = true;
    const response = await api.task.updateCompleted(id, payload);
    if (response.success) {
      await getTasks();
    }
    loading.value = false;
  };
  const getTaskById = async (id) => {
    loading.value = true;
    const response = await api.task.getById(id);
    if (response.success) {
      loading.value = false;
      return response.data;
    } else {
      //TODO: handler error
    }
    loading.value = false;
  };
  const deleteTaskById = async (id) => {
    result.value = "";
    loading.value = true;
    const response = await api.task.delete(id);
    if (response.success) {
      await getTasks();
    }
    loading.value = false;
  };
  return {
    getTasks,
    loading,
    result,
    errorMessage,
    createTask,
    updateTask,
    updateCompletedTask,
    getTaskById,
    deleteTaskById,
  };
};
